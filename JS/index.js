const firstDropdown = document.querySelector('.select__arrow')
const firstList = document.querySelector('.select__body')
const secDropdown = document.querySelector('.select__arrow-sec')
const secList  = document.querySelector('.select__body-sec')

firstDropdown.addEventListener('click', () => {
  firstList.classList.toggle('show')
  firstDropdown.classList.toggle('active')
})

secDropdown.addEventListener('click', function () {
    secList.classList.toggle('show')
    secDropdown.classList.toggle('active')
})